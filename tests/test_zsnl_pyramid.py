# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.exceptions import Conflict, Forbidden, NotFound
from sqlalchemy.dialects import postgresql
from unittest import mock
from zsnl_pyramid import platform_key


def test_platform_key_includeme():
    mock_config = mock.MagicMock()

    platform_key.includeme(mock_config)

    call_list = mock_config.add_request_method.call_args_list
    assert call_list[0] == mock.call(
        platform_key._get_platform_user_uuid, "get_platform_user_uuid"
    )
    assert call_list[1] == mock.call(
        platform_key._assert_platform_key, "assert_platform_key"
    )


def test_assert_platform_key_success():
    mock_request = mock.MagicMock()
    mock_request.configuration = {"zs_platform_key": "aaa"}
    mock_request.headers = {"zs-platform-key": "aaa"}

    assert platform_key._assert_platform_key(mock_request) is None


def test_assert_platform_key_failure():
    mock_request = mock.MagicMock()
    mock_request.configuration = {"zs_platform_key": "aaa"}
    mock_request.headers = {"zs-platform-key": "bbb"}

    with pytest.raises(Forbidden):
        platform_key._assert_platform_key(mock_request)


def test_get_platform_user_uuid_admin():
    mock_request = mock.MagicMock()
    mock_request.headers = {}
    mock_row = mock.MagicMock()

    mock_row.uuid = 307955199
    mock_row.permissions = {"pip_user": False}

    mock_request.get_infrastructure_ro().execute().fetchall.return_value = [
        mock_row
    ]

    uuid, permissions = platform_key._get_platform_user_uuid(mock_request)

    assert uuid == "307955199"
    assert permissions == {"pip_user": False}
    mock_request.get_infrastructure_ro().close.assert_called_once_with()

    query = mock_request.get_infrastructure_ro().execute.call_args[0][0]
    compiled_query = query.compile(dialect=postgresql.dialect())

    assert str(compiled_query) == (
        "SELECT subject.uuid \n"
        "FROM subject JOIN user_entity ON user_entity.subject_id = subject.id JOIN interface ON interface.id = user_entity.source_interface_id \n"
        "WHERE subject.username = %(username_1)s AND interface.module = %(module_1)s AND user_entity.date_deleted IS NULL"
    )
    assert compiled_query.params == {
        "username_1": "admin",
        "module_1": "authldap",
    }


def test_get_platform_user_uuid_person():
    mock_request = mock.MagicMock()
    mock_request.headers = {"zs-user-override": "person:307955199"}
    mock_row = mock.MagicMock()

    mock_row.uuid = 307955199
    mock_row.permissions = {"pip_user": True}

    mock_request.get_infrastructure_ro().execute().fetchall.return_value = [
        mock_row
    ]

    uuid, permissions = platform_key._get_platform_user_uuid(mock_request)

    assert uuid == "307955199"
    assert permissions == {"pip_user": True}
    mock_request.get_infrastructure_ro().close.assert_called_once_with()

    query = mock_request.get_infrastructure_ro().execute.call_args[0][0]
    compiled_query = query.compile(dialect=postgresql.dialect())

    assert str(compiled_query) == (
        "SELECT natuurlijk_persoon.uuid \n"
        "FROM natuurlijk_persoon \n"
        "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.burgerservicenummer = %(burgerservicenummer_1)s"
    )
    assert compiled_query.params == {
        "burgerservicenummer_1": "307955199",
    }


def test_get_platform_user_uuid_organization():
    mock_request = mock.MagicMock()
    mock_request.headers = {"zs-user-override": "organization:rsin:307955199"}
    mock_row = mock.MagicMock()

    mock_row.uuid = 307955199
    mock_row.permissions = {"pip_user": True}

    mock_request.get_infrastructure_ro().execute().fetchall.return_value = [
        mock_row
    ]

    uuid, permissions = platform_key._get_platform_user_uuid(mock_request)

    assert uuid == "307955199"
    assert permissions == {"pip_user": True}
    mock_request.get_infrastructure_ro().close.assert_called_once_with()

    query = mock_request.get_infrastructure_ro().execute.call_args[0][0]
    compiled_query = query.compile(dialect=postgresql.dialect())

    assert str(compiled_query) == (
        "SELECT bedrijf.uuid \n"
        "FROM bedrijf \n"
        "WHERE bedrijf.deleted_on IS NULL AND bedrijf.rsin = %(rsin_1)s"
    )
    assert compiled_query.params == {
        "rsin_1": "307955199",
    }


def test_get_platform_user_uuid_organization_vestigingsnummer():
    mock_request = mock.MagicMock()
    mock_request.headers = {
        "zs-user-override": "organization:vestigingsnummer:307955199"
    }
    mock_row = mock.MagicMock()

    mock_row.uuid = 307955199
    mock_row.permissions = {"pip_user": True}

    mock_request.get_infrastructure_ro().execute().fetchall.return_value = [
        mock_row
    ]

    uuid, permissions = platform_key._get_platform_user_uuid(mock_request)

    assert uuid == "307955199"
    assert permissions == {"pip_user": True}
    mock_request.get_infrastructure_ro().close.assert_called_once_with()

    query = mock_request.get_infrastructure_ro().execute.call_args[0][0]
    compiled_query = query.compile(dialect=postgresql.dialect())

    assert str(compiled_query) == (
        "SELECT bedrijf.uuid \n"
        "FROM bedrijf \n"
        "WHERE bedrijf.deleted_on IS NULL AND bedrijf.vestigingsnummer = %(vestigingsnummer_1)s"
    )
    assert compiled_query.params == {
        "vestigingsnummer_1": "307955199",
    }


def test_get_platform_user_uuid_not_found():
    mock_request = mock.MagicMock()
    mock_request.headers = {}

    mock_request.get_infrastructure_ro().execute().fetchall.return_value = []

    with pytest.raises(NotFound):
        platform_key._get_platform_user_uuid(mock_request)


def test_get_platform_user_uuid_too_many():
    mock_request = mock.MagicMock()
    mock_request.headers = {}
    mock_request.get_infrastructure_ro().execute().fetchall.return_value = [
        ("a",),
        ("b",),
    ]

    with pytest.raises(Conflict):
        platform_key._get_platform_user_uuid(mock_request)


def test_get_platform_user_uuid_wrong_header():
    mock_request = mock.MagicMock()
    mock_request.headers = {"zs-user-override": "organization:307955199"}
    mock_request.get_infrastructure_ro().execute().fetchall.return_value = []

    with pytest.raises(Conflict):
        platform_key._get_platform_user_uuid(mock_request)


@mock.patch("zsnl_domains.case_management.entities._shared.is_valid_bsn")
def test_get_platform_user_uuid_wrong_bsn(valid_bsn):
    valid_bsn.return_value = False
    mock_request = mock.MagicMock()
    mock_request.headers = {"zs-user-override": "person:1122"}
    mock_request.get_infrastructure_ro().execute().fetchall.return_value = []

    with pytest.raises(Conflict):
        platform_key._get_platform_user_uuid(mock_request)


def test_default_dict():
    def_dict = platform_key.default_true_dict(special_key=False)

    assert def_dict.get("value") is True
    assert def_dict.get("special_key") is False
