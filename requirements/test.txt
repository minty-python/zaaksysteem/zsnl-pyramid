## QA (tests, documentation)
pytest~=7.0
pytest-cov~=4.0
pytest-mock~=3.3
pytest-spec~=3.0

## Code style tools
black~=22.1
flake8~=3.8
isort~=5.5
mypy~=0.761

## QA (tests, documentation, security, licenses)
pip-audit
liccheck~=0.1
